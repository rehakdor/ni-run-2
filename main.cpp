#include <iostream>
#include <exception>
#include <fstream>
#include <string>
#include <cstdint>
#include <cstdlib>

#include "ast.hpp"
#include "convertor.hpp"
#include "interpreter.hpp"

int main(int argc, char *argv[]){
    std::cout << "Start" << std::endl;
    if(argc < 2)
        std::cerr << "Missing argument" << std::endl;

    try{
        std::ifstream input_file(argv[1]);
        input_file.exceptions(std::istream::eofbit | std::istream::failbit | std::istream::badbit);
        ConvertorJSON convertor;
        std::unique_ptr<ASTNode> top(convertor.convert(input_file));
        input_file.close();

        Interpret ip;
        ip.runTop(static_cast<ASTTop&>(*top));
    }catch(std::exception& e){
        std::cerr << e.what() << std::endl;
        goto fail;
    }catch(...){
        std::cerr << "Unknown object thrown" << std::endl;
        goto fail;
    }

    std::cout << "Success" << std::endl;
    return 0;

    fail:
    std::cout << "Failure" << std::endl;
    return 1;
}
