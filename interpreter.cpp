#include "interpreter.hpp"

#include <map>
#include <string>
#include <stdexcept>
#include <iostream>

#include "ast.hpp"
#include "util.hpp"
#include "exception.hpp"

void Object::take(void){
    refcount++;
}
void Object::release(void){
    refcount--;
    if(refcount == 0)
        delete this;
}
Object::Type ObjectInteger::getType(void) const{
    return Type::Integer;
}
Object::Type ObjectBoolean::getType(void) const{
    return Type::Boolean;
}
Object::Type ObjectNull::getType(void) const{
    return Type::Null;
}
ObjectNull singleton_null; // will always have refcount > 0 unless bug
ObjectNull *ObjectNull::singleton(void){
    singleton_null.take();
    return &singleton_null;
}
Object::Type ObjectFunction::getType(void) const{
    return Type::Function;
}


Frame::~Frame(void){
    for(auto it = mapping.begin(); it != mapping.end(); ++it)
        it->second->release();
}
bool Frame::getMappingIt(map_it& output, const std::string& name){
    map_it it = mapping.find(name);
    if(it != mapping.end()){
        output = it;
        return true;
    }
    if(parent)
        return parent->getMappingIt(output, name);
    return false;
}
Object *Frame::getMapping(const std::string& name){
    map_it it;
    if(!getMappingIt(it, name))
        return nullptr;
    return it->second;
}
void Frame::setMapping(const std::string& name, Object& obj){
    map_it it;
    if(!getMappingIt(it, name)){
        mapping.emplace(name, &obj);
    }else{
        it->second->release();
        it->second = &obj;
    }
    obj.take();
}
void Frame::setMappingDirect(const std::string& name, Object& obj){
    map_it it = mapping.find(name);
    if(it == mapping.end())
        mapping.emplace(name, &obj);
    else{
        it->second->release();
        it->second = &obj;
    }
    obj.take();
}


Object *Interpret::runDispatch(const ASTNode& node, Frame& curr_frame){
    switch(node.getType()){
        case ASTNode::Type::Integer:
            return runInteger(static_cast<const ASTInteger&>(node));
        case ASTNode::Type::Boolean:
            return runBoolean(static_cast<const ASTBoolean&>(node));
        case ASTNode::Type::Null:
            return ObjectNull::singleton();
        case ASTNode::Type::Variable:
            return runVariable(static_cast<const ASTVariable&>(node), curr_frame);
        case ASTNode::Type::AccessVariable:
            return runAccessVariable(static_cast<const ASTAccessVariable&>(node), curr_frame);
        case ASTNode::Type::AssignVariable:
            return runAssignVariable(static_cast<const ASTAssignVariable&>(node), curr_frame);
        case ASTNode::Type::Function:
            return runFunction(static_cast<const ASTFunction&>(node), curr_frame);
        case ASTNode::Type::CallFunction:
            return runCallFunction(static_cast<const ASTCallFunction&>(node), curr_frame);
        case ASTNode::Type::Print:
            return runPrint(static_cast<const ASTPrint&>(node), curr_frame);
        case ASTNode::Type::Block:
            return runBlock(static_cast<const ASTBlock&>(node), curr_frame);
        case ASTNode::Type::Loop:
            return runLoop(static_cast<const ASTLoop&>(node), curr_frame);
        case ASTNode::Type::Conditional:
            return runConditional(static_cast<const ASTConditional&>(node), curr_frame);
        default:
            MY_ASSERT(false); // cannot happen
    }
}
void Interpret::runTop(const ASTTop& top){
    Frame top_frame;
    for(const ASTNode *n : top.children)
        runDispatch(*n, top_frame)->release();
}
Object *Interpret::runInteger(const ASTInteger& integer){
    return new ObjectInteger(integer.value);
}
Object *Interpret::runBoolean(const ASTBoolean& boolean){
    return new ObjectBoolean(boolean.value);
}
Object *Interpret::runVariable(const ASTVariable& variable, Frame& curr_frame){
    Object *value = runDispatch(*variable.value, curr_frame);
    curr_frame.setMappingDirect(variable.name, *value);
    return value;
}
Object *Interpret::runAccessVariable(const ASTAccessVariable& access_variable, Frame& curr_frame){
    Object *obj = curr_frame.getMapping(access_variable.name);
    if(!obj)
        throw SemanticError("undeclared variable");
    obj->take();
    return obj;
}
Object *Interpret::runAssignVariable(const ASTAssignVariable& assign_variable, Frame& curr_frame){
    Object *value = runDispatch(*assign_variable.value, curr_frame);
    curr_frame.setMapping(assign_variable.name, *value);
    return value;
}
Object *Interpret::runFunction(const ASTFunction& function, Frame& curr_frame){
    Object *value = new ObjectFunction(function);
    curr_frame.setMappingDirect(function.name, *value);
    value->release();
    return ObjectNull::singleton();
}
Object *Interpret::runCallFunction(const ASTCallFunction& call_function, Frame& curr_frame){
    Object *obj = curr_frame.getMapping(call_function.name);
    if(!obj)
        throw SemanticError("function not found");
    if(obj->getType() != Object::Type::Function)
        throw SemanticError("calling something not a function");
    const ObjectFunction& real_obj = static_cast<const ObjectFunction&>(*obj);
    std::vector<Object *> arguments;
    for(const ASTNode *node : call_function.arguments)
        arguments.push_back(runDispatch(*node, curr_frame));
    Frame function_frame(curr_frame);
    for(size_t i = 0; i < call_function.arguments.size(); i++){
        function_frame.setMappingDirect(real_obj.function.parameters[i], *arguments[i]);
        arguments[i]->release(); // they are now taken within function frame
    }
    return runDispatch(*real_obj.function.body, function_frame);
}
void Interpret::printObject(const Object& obj){
    switch(obj.getType()){
        case Object::Type::Integer:
            std::cout << static_cast<const ObjectInteger&>(obj).value;
            break;
        case Object::Type::Boolean:
            std::cout << (static_cast<const ObjectBoolean&>(obj).value ? "true" : "false");
            break;
        case Object::Type::Null:
            std::cout << "null";
            break;
        default:
            throw SemanticError("Unsupported print type");
    }
}
#define BACKLASH_ITEM(input, output) \
    case input: \
    std::cout << output; \
    break
Object *Interpret::runPrint(const ASTPrint& print, Frame& curr_frame){
    std::vector<Object *> arguments;
    for(const ASTNode *node : print.arguments){
        arguments.push_back(runDispatch(*node, curr_frame));
    }
    const char *c_str = print.format.c_str();
    size_t i = 0;
    size_t curr_arg = 0;
    bool backlash_mode = false;
    for(char curr_char; (curr_char = c_str[i]) != '\0'; i++){
        if(backlash_mode){
            switch(curr_char){
                BACKLASH_ITEM('~', "~");
                BACKLASH_ITEM('n', std::endl);
                BACKLASH_ITEM('"', "\"");
                BACKLASH_ITEM('t', "\t");
                BACKLASH_ITEM('\\', "\\");
                case 'r':
                    break; // doesnt work in C++
                default:
                    throw SemanticError("Unknown \\ operator");
            }
            backlash_mode = false;
        }else{
            if(curr_char == '\\')
                backlash_mode = true;
            else if(curr_char == '~'){
                printObject(*arguments[curr_arg]);
                curr_arg++;
            }else
                std::cout << curr_char;
        }
    }
    if(backlash_mode)
        throw SemanticError("Print cannot end with \\");
    for(auto node : arguments)
        node->release();
    return ObjectNull::singleton();
}
Object *Interpret::runBlock(const ASTBlock& block, Frame& curr_frame){
    Frame block_frame(curr_frame);
    std::vector<Object *> children;
    for(const ASTNode *node : block.children)
        children.push_back(runDispatch(*node, block_frame));
    if(children.size() == 0)
        return ObjectNull::singleton();
    else{
        for(size_t i = 0; i < children.size() - 1; i++) // release all except last one
            children[i]->release();
        return children.back();
    }
}
bool Interpret::evaluatesTrue(const Object& input){
    if(input.getType() == Object::Type::Boolean){
        const ObjectBoolean& real_input = static_cast<const ObjectBoolean&>(input);
        return real_input.value;
    }
    if(input.getType() == Object::Type::Null)
        return false;
    return true;
}
Object *Interpret::runLoop(const ASTLoop& loop, Frame& curr_frame){
    while(true){
        Object *cond_val = runDispatch(*loop.condition, curr_frame);
        bool t = evaluatesTrue(*cond_val);
        cond_val->release();
        if(!t)
            return ObjectNull::singleton();
        runDispatch(*loop.body, curr_frame)->release();
    }
}
Object *Interpret::runConditional(const ASTConditional& conditional, Frame& curr_frame){
    Object *cond_val = runDispatch(*conditional.condition, curr_frame);
    bool t = evaluatesTrue(*cond_val);
    cond_val->release();
    if(t)
        return runDispatch(*conditional.consequent, curr_frame);
    else
        return runDispatch(*conditional.alternative, curr_frame);
}
