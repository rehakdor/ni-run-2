#ifndef UTIL_HPP
#define UTIL_HPP

#include <iostream>

#define MY_ASSERT(cond) \
do{ \
    if(!(cond)){ \
        std::cerr << "Assertion failed: " << #cond << " (" << __LINE__ << ")" << std::endl; \
        std::abort(); \
    } \
}while(0)

#endif
