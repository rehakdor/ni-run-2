#include "ast.hpp"

#include <vector>
#include <cstdint>
#include <string>

ASTNode::Type ASTInteger::getType(void) const{
    return Type::Integer;
}

ASTNode::Type ASTBoolean::getType(void) const{
    return Type::Boolean;
}

ASTNode::Type ASTNull::getType(void) const{
    return Type::Null;
}

ASTVariable::~ASTVariable(void){
    delete value;
}
ASTNode::Type ASTVariable::getType(void) const{
    return Type::Variable;
}

ASTNode::Type ASTAccessVariable::getType(void) const{
    return Type::AccessVariable;
}

ASTAssignVariable::~ASTAssignVariable(void){
    delete value;
}
ASTNode::Type ASTAssignVariable::getType(void) const{
    return Type::AssignVariable;
}

ASTFunction::~ASTFunction(void){
    delete body;
}
ASTNode::Type ASTFunction::getType(void) const{
    return Type::Function;
}

ASTCallFunction::~ASTCallFunction(void){
    for(ASTNode *argument : arguments)
        delete argument;
}
ASTNode::Type ASTCallFunction::getType(void) const{
    return Type::CallFunction;
}

ASTPrint::~ASTPrint(void){
    for(ASTNode *argument : arguments)
        delete argument;
}
ASTNode::Type ASTPrint::getType(void) const{
    return Type::Print;
}

ASTBlock::~ASTBlock(void){
    for(ASTNode *child : children)
        delete child;
}
ASTNode::Type ASTBlock::getType(void) const{
    return Type::Block;
}

ASTTop::~ASTTop(void){
    for(ASTNode *child : children)
        delete child;
}
ASTNode::Type ASTTop::getType(void) const{
    return Type::Top;
}

ASTLoop::~ASTLoop(void){
    delete condition;
    delete body;
}
ASTNode::Type ASTLoop::getType(void) const{
    return Type::Loop;
}

ASTConditional::~ASTConditional(void){
    delete condition;
    delete consequent;
    delete alternative;
}
ASTNode::Type ASTConditional::getType(void) const{
    return Type::Conditional;
}
