#ifndef INTERPRETER_HPP
#define INTERPRETER_HPP

#include <map>
#include <string>

#include "ast.hpp"

class Object{
protected:
    unsigned int refcount;

public:
    enum class Type{
        Integer,
        Boolean,
        Null,
        Function,
    };

    Object(void)
    : refcount(1){
    }
    virtual ~Object(void) = default;
    virtual Type getType(void) const = 0;

    void take(void);
    void release(void);
};
class ObjectInteger: public Object{
public:
    uint32_t value;

    ObjectInteger(uint32_t pvalue)
    : value(pvalue){
    };
    Type getType(void) const override;
};
class ObjectBoolean: public Object{
public:
    bool value;

    ObjectBoolean(bool pvalue)
    : value(pvalue){
    };
    Type getType(void) const override;
};
class ObjectNull: public Object{
public:
    Type getType(void) const override;

    static ObjectNull *singleton(void);
};
class ObjectFunction: public Object{
public:
    const ASTFunction& function;

    ObjectFunction(const ASTFunction& pfunction)
    : function(pfunction){
    }
    Type getType(void) const override;
};

class Frame{
public:
    Frame *parent;
    std::map<std::string, Object *> mapping;

    Frame(void)
    : parent(nullptr){
    }
    Frame(Frame& pparent)
    : parent(&pparent){
    }
    ~Frame(void);

    using map_it = typename std::map<std::string, Object *>::iterator;
    bool getMappingIt(map_it& output, const std::string& name);
    Object *getMapping(const std::string& name);
    void setMapping(const std::string& name, Object& obj);
    void setMappingDirect(const std::string& name, Object& obj);
};

class Interpret{
public:
    void runTop(const ASTTop& top);

protected:
    Object *runDispatch(const ASTNode& node, Frame& curr_frame);
    Object *runInteger(const ASTInteger& integer);
    Object *runBoolean(const ASTBoolean& boolean);
    Object *runVariable(const ASTVariable& variable, Frame& curr_frame);
    Object *runAccessVariable(const ASTAccessVariable& access_variable, Frame& curr_frame);
    Object *runAssignVariable(const ASTAssignVariable& assign_variable, Frame& curr_frame);
    Object *runFunction(const ASTFunction& function, Frame& curr_frame);
    Object *runCallFunction(const ASTCallFunction& call_function, Frame& curr_frame);
    void printObject(const Object& obj);
    Object *runPrint(const ASTPrint& print, Frame& curr_frame);
    Object *runBlock(const ASTBlock& block, Frame& curr_frame);
    bool evaluatesTrue(const Object& input);
    Object *runLoop(const ASTLoop& loop, Frame& curr_frame);
    Object *runConditional(const ASTConditional& conditional, Frame& curr_frame);
};

#endif
