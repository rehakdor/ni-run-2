Dependencies:
* C++11 compiler
  * Default uses clang, change clang++ to g++ in Makefile if you use GCC
* libjsoncpp
  * Package on Debian/Ubuntu: libjsoncpp-dev

Some test files are in /tests, turn them into JSONs using the jsonize.sh (first argument is path to FML binary; run from within /tests).

In case of emergency:
If the interpreter crashes in your test cases, try replacing the "delete" at interpreter.cpp:18 with ";" (its an object reference counter). It will memleak but continue to compute.
The reference counter works in all my tests, but I didnt have much time to test it thoroughly, therefore this notice.
