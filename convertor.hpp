#ifndef CONVERTOR_HPP
#define CONVERTOR_HPP

#include <fstream>

#include <jsoncpp/json/json.h>

#include "ast.hpp"

class Convertor{
public:
    virtual ~Convertor(void) = default;
    virtual ASTNode *convert(std::ifstream& stream) = 0;
};

class ConvertorJSON: public Convertor{
public:
    ASTNode *convert(std::ifstream& stream) override;

protected:
    ASTNode *convertDispatch(const Json::Value& json);
    ASTInteger *convertInteger(const Json::Value& json);
    ASTBoolean *convertBoolean(const Json::Value& json);
    ASTNull *convertNull(void);
    ASTVariable *convertVariable(const Json::Value& json);
    ASTAccessVariable *convertAccessVariable(const Json::Value& json);
    ASTAssignVariable *convertAssignVariable(const Json::Value& json);
    ASTFunction *convertFunction(const Json::Value& json);
    ASTCallFunction *convertCallFunction(const Json::Value& json);
    ASTPrint *convertPrint(const Json::Value& json);
    ASTBlock *convertBlock(const Json::Value& json);
    ASTTop *convertTop(const Json::Value& json);
    ASTLoop *convertLoop(const Json::Value& json);
    ASTConditional *convertConditional(const Json::Value& json);
};

#endif
